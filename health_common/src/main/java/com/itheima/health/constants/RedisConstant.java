package com.itheima.health.constants;

/**
 * 消息常量
 */
public class RedisConstant {
    public static final String SETMEAL_PIC_RESOURCES = "setmealPicResources"; // 上传图片
    public static final String SETMEAL_PIC_DB_RESOURCES = "setmealPicDbResources"; // 添加套餐
    public static final String SETMEAL_LIST_CACHE = "setMealListCache"; // 套餐列表的缓存
    public static final String SETMEAL_CACHE = "setMealCache"; // 套餐信息的缓存
}
