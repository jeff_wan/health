package com.itheima.health.aspect.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 添加这个注解, 将删除redis的缓存,
 * @author zl
 * @version 1.0
 * @date 2019/10/28 19:27
 */
//没做功能
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisCacheUpdate {
    //需要更新的prefix key
    String value();

    //需要更新的id值, 如果没有的话必须填传入的bean类
    int idIndex() default -1;

    //如果没有直接写id, 则需要这个class,并且class对象必须有getId()方法
    int updateBeanIndex() default -1  ;
}
