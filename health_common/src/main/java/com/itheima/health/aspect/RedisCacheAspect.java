package com.itheima.health.aspect;

import com.alibaba.fastjson.JSONObject;
import com.itheima.health.aspect.annotation.RedisCacheAdd;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.lang.reflect.Method;

/**
 * @author zl
 * @version 1.0
 * @date 2019/10/27 15:55
 */
public class RedisCacheAspect {
    @Autowired
    private JedisPool jedisPool;

    /**
     * 数据缓存的增强方法
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    public Object redisCache(JoinPoint joinPoint) throws Throwable {
        //获取方法上的注解
        String methodName = joinPoint.getSignature().getName();
        Class[] parameterTypes = ((MethodSignature) joinPoint.getSignature()).getParameterTypes();
        Method method = joinPoint.getTarget().getClass().getMethod(methodName, parameterTypes);

        RedisCacheAdd redisCache = method.getAnnotation(RedisCacheAdd.class);
        if (redisCache != null) {
            //如果有注解, 执行缓存操作,返回数据
            String value = redisCache.value();
            return doRedisCache(value, joinPoint);
        }
        return ((ProceedingJoinPoint) joinPoint).proceed();
    }

    /**
     * 执行redis缓存方法
     *
     * @param redisCacheKey 缓存的key
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    private Object doRedisCache(String redisCacheKey, JoinPoint joinPoint) throws Throwable {
        //获取参数, 如果有参数的话, 比如findById(Integer id) 那就需要在key后面加上id储存,获取
        Object[] args = joinPoint.getArgs();
        if (args.length > 0) {
            redisCacheKey += args[0];
        }

        Jedis jedis = jedisPool.getResource();
        String redisCacheStr = jedis.get(redisCacheKey);
        if (redisCacheStr == null) {
            //执行目标方法获取返回值
            Object returnObj = ((ProceedingJoinPoint) joinPoint).proceed(args);
            if (returnObj != null) {
                //转换成json字符串,存入redis
                redisCacheStr = JSONObject.toJSONString(returnObj);
                jedis.set(redisCacheKey, redisCacheStr);
                //一小时之后过期
                jedis.expire(redisCacheKey, 60 * 60);

            }
        }
        jedis.close();
        return redisCacheStr;
    }
}
