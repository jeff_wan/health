package com.itheima.health.aspect.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 添加这个注解, 将使用redis缓存数据
 * value 为存储的redis的键名
 * @author zl
 * @version 1.0
 * @date 2019/10/27 18:33
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisCacheAdd {
    String value();
}
