package com.itheima.health.job;

import com.itheima.health.dao.OrderSettingDao;
import com.itheima.health.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;

public class ClearOrderSettingJob {

    @Autowired
    private OrderSettingDao orderSettingDao;
    // 使用Quartz清理图片
    public void clearOrdersetting(){
        try {
            //获取当前时间前一个月的日期
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH,-1);
            String preMonth = DateUtils.parseDate2String(calendar.getTime());
            //执行删除功能,删除小于参数时间的数据
            orderSettingDao.deleteByOrderDate(preMonth);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
