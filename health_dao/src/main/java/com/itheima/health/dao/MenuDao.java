package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CheckItemDao
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:53
 * @Version V1.0
 */
public interface MenuDao {

    Page<Menu> findPageMenu(String queryString);

    void addMenu(Menu menu);

    Menu findMenuById(Integer id);

    void editMenu(Menu menu);

    long findRoleAndMenuByMenuId(Integer id);

    void deleteById(Integer id);

    List<Menu> findMenuAll();

    List<Menu> getMenu(String username);

    LinkedHashSet<Menu> findMenusByRoleId(Integer id);
    List<Menu> findSecondMenusByParentMenuId(Integer id);
}
