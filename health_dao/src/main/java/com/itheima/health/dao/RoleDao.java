package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @ClassName CheckItemDao
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:53
 * @Version V1.0
 */
public interface RoleDao {

    // 根据用户id，查询角色的集合
    Set<Role> findRolesByUserId(Integer userId);

    List<Role> findAll();

    Page<Role> findPage(String queryString);

    void addRole(Role role);

    void addRoleAndPermission(@Param("pid") int permissionId, @Param("rid") Integer rid);

    void addRoleAndMenu(@Param("mid") int menuId, @Param("rid") Integer rid);

    Role findRoleById(Integer id);

    List<Integer> findPermissionByRoleId(Integer id);

    List<Integer> findMenuByRoleId(Integer id);

    long findRoleAndPermissionByRoleId(Integer id);

    long findRoleAndMenuByRoleId(Integer id);

    void deleteRoleById(Integer id);

    void updateRole(Role role);

    void deleteRoleAndPermissionByRoleId(Integer rid);

    void deleteRoleAndMenuByRoleId(Integer rid);
}
