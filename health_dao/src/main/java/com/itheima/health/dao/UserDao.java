package com.itheima.health.dao;

import com.itheima.health.pojo.User;

import java.util.List;

/**
 * @ClassName CheckItemDao
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:53
 * @Version V1.0
 */
public interface UserDao {

    User findUserByUsername(String username);

    List<User> findPage(String queryString);

    User findById(Integer id);

    void edit(User user);

    void add(User user);

    long findCheckGroupAndCheckItemByCheckItemId(Integer id);

    void deleteById(Integer id);
}
