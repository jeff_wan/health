package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.MenuDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Menu;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;
import com.itheima.health.service.MenuService;
import com.itheima.health.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * @ClassName CheckItemServiceImpl
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:54
 * @Version V1.0
 */
@Service(interfaceClass = MenuService.class)
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    MenuDao menuDao;

    @Autowired
    UserService userService;

    @Override
    public PageResult findPageMenu(Integer currentPage, Integer pageSize, String queryString) {
        //分页初始化
        PageHelper.startPage(currentPage,pageSize);
        Page<Menu> page= menuDao.findPageMenu(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public void addMenu(Menu menu) {
        menuDao.addMenu(menu);
    }

    @Override
    public Menu findMenuById(Integer id) {
        return menuDao.findMenuById(id);
    }

    @Override
    public void editMenu(Menu menu) {
        menuDao.editMenu(menu);
    }

    @Override
    public void deleteMenuById(Integer id) {
        // 在删除之前，判断检查组和检查项中间表中是否存在数据，有数据不能删除，没有数据可以删除
        long count = menuDao.findRoleAndMenuByMenuId(id);
        // 此时存在数据，不能删除，提示用户
        if(count>0){
            throw new RuntimeException("中间表和检查组中存在关联数据，不能删除");
        }
        // 删除检查项
        menuDao.deleteById(id);
    }

    @Override
    public List<Menu> findMenuAll() {
        return menuDao.findMenuAll();
    }

    //处理请求获取菜单集合
    @Override
    public List<Menu> getMenuList(String username) {
        //List<Map<String,Object>> result = new ArrayList<>();
        //
        //List<Map<String,Object>> oneMenuList = menuDao.findLevelOneMenuByUsername(username);
        //
        //for (Map<String, Object> map : oneMenuList) {
        //    Map<String,Object> menu = new HashMap<>(4);
        //    menu.put("path",map.get("path"));
        //    menu.put("title",map.get("title"));
        //    menu.put("icon",map.get("icon"));
        //
        //    Map<String, Object> tempMap = new HashMap<>(2);
        //    Integer parentMenuId = (Integer) map.get("id");
        //    tempMap.put("username",username);
        //    tempMap.put("parentMenuId",parentMenuId);
        //    List<Map<String,Object>> twoMenu = menuDao.findLevelTwoMenuByUsername(tempMap);
        //    menu.put("children",twoMenu);
        //    result.add(menu);
        //}
        //
        //return result;
        // return oneMenuList;
    //    User user = userService.findUserByUsername(username);
    //    Set<Role> roles = user.getRoles();
    //    List<Menu> list = new ArrayList<>();
    //    if (roles!=null){
    //        for (Role role : roles) {
    //            LinkedHashSet<Menu> menus = role.getMenus();
    //            for (Menu menu : menus) {
    //                list.add(menu);
    //            }
    //        }
    //    }
    //    return list;
        List<Menu> menu = menuDao.getMenu(username);
        List<Menu> parentMenus = new ArrayList<>();
        Boolean flag = false;
        for (Menu menu1 : menu) {
            for (Menu parentMenu : parentMenus) {
                if(menu1.getParentMenuId() == parentMenu.getId()){
                    flag = true;
                    parentMenu.getChildren().add(menu1);
                    break;
                }
            }
            if(!flag){
                Menu parentMenu = menuDao.findMenuById(menu1.getParentMenuId());
                parentMenu.getChildren().add(menu1);
                parentMenus.add(parentMenu);
            }
            flag = false;
        }
        return parentMenus;
    }
}
