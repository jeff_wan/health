package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import com.itheima.health.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CheckItemServiceImpl
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:54
 * @Version V1.0
 */
@Service(interfaceClass = MemberService.class)
@Transactional
public class MemberServiceImpl implements MemberService {

    // 会员
    @Autowired
    MemberDao memberDao;


    @Override
    public Member findMemberByTelephone(String telephone) {
        return memberDao.findMemberByTelephone(telephone);
    }

    @Override
    public void add(Member member) {
        // 对密码进行md5加密保存
        if(member.getPassword()!=null){
            member.setPassword(MD5Utils.md5(member.getPassword()));
        }
        memberDao.add(member);
    }

    @Override
    public List<Integer> findMemberCountByRegTime(List<String> monthsList) {
        // 组织查询结果
        List<Integer> memberCountList = new ArrayList<>();
        for (String month : monthsList) {
            String regTime = month+"-31"; // 已知当前月，查询当前月的最后1天
            Integer memberCount = memberDao.findMemberCountByRegTime(regTime);
            memberCountList.add(memberCount);
        }
        return memberCountList;
    }

    @Override
    public List<Map<String, Object>> findMemberSexReport() {
        List<Map<String, Object>> memberSexReport =  memberDao.findMemberSexReport();
        return memberSexReport;
    }

    @Override
    public List<Map<String, Object>> findMemberAgeReport() {
        List<Map<String, Object>> memberAgeReport =  memberDao.findMemberAgeReport();
        return memberAgeReport;
    }

    @Override
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        // 方案一：如果不使用mybatis的分页插件
        // 总记录数：（sql：select count(*) from t_checkitem where name=#{queryString}）
        // 当前页的查询集合List：（sql：select * from t_checkitem where name=#{queryString} limit ?,?）
        // 第一个?表示当前页从第几条开始检索（起始记录数）：计算(currentPage-1)*pageSize
        // 第二个?表示当前页最多显示的记录数：计算pageSize
        // 方案二：使用mybatis的分页插件
        // 1：初始化参数
        PageHelper.startPage(currentPage,pageSize);
        // 2：查询数据
        List<Map<String,String>> list = memberDao.findPage(queryString);
        // 3：封装PageHelp对应的结果集
        PageInfo<Map<String,String>> pageInfo = new PageInfo<>(list);
        // 封装数据
        return new PageResult(pageInfo.getTotal(),pageInfo.getList());
        // 或者使用
//        Page<CheckItem> page = checkItemDao.findPage(queryString);
//        return new PageResult(page.getTotal(),page.getResult());

    }

    @Override
    public Member findById(Integer id) {
        return memberDao.findById(id);
    }

    @Override
    public void edit(Member member) {
        memberDao.edit(member);
    }

    @Override
    public void delete(Integer id) {
        // 删除会员
        memberDao.deleteById(id);
    }
}
