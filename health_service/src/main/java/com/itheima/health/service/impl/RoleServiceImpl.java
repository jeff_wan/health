package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.constants.MessageConstant;
import com.itheima.health.dao.RoleDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.Role;
import com.itheima.health.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @ClassName CheckItemServiceImpl
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:54
 * @Version V1.0
 */
@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleDao roleDao;

    @Override
    public PageResult findAllRole(Integer currentPage, Integer pageSize, String queryString) {
        //分页初始化
        PageHelper.startPage(currentPage,pageSize);
        Page<Role> page= roleDao.findPage(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public void addRole(int[] permissionIds, int[] menuIds, Role role) {
        roleDao.addRole(role);
        addRoleAndPermission(permissionIds,role.getId());
        addRoleAndMenu(menuIds,role.getId());
    }

    @Override
    public Role findRoleById(Integer id) {
        return roleDao.findRoleById(id);
    }

    @Override
    public List<Integer> findPermissionByRoleId(Integer id) {
        return roleDao.findPermissionByRoleId(id);
    }

    @Override
    public List<Integer> findMenuByRoleId(Integer id) {
        return roleDao.findMenuByRoleId(id);
    }

    @Override
    public void updateRole(Role role, int[] permissionIds, int[] menuIds) {
        addRoleAndPermission(permissionIds,role.getId());
        addRoleAndMenu(menuIds,role.getId());
        roleDao.updateRole(role);
    }

    @Override
    public void deleteRoleById(Integer id) {
        //先判断是否需有绑定
        //先查询权限
        long countWithPermission = roleDao.findRoleAndPermissionByRoleId(id);
        long countWithMenu = roleDao.findRoleAndMenuByRoleId(id);
        if(countWithMenu>0 || countWithPermission>0){
            throw new RuntimeException("中间表和检查组中存在关联数据，不能删除");
        }
        roleDao.deleteRoleById(id);
    }

    //遍历permissionIds数组，获取角色id，同时和权限id，向角色和权限的中间表插入
    private void addRoleAndPermission(int[] permissionIds,Integer rid){
        //删除原有连接（编辑修改不清楚添加不进去）
        roleDao.deleteRoleAndPermissionByRoleId(rid);
        if (permissionIds != null && permissionIds.length > 0) {
            for (int permissionId : permissionIds) {
                roleDao.addRoleAndPermission(permissionId,rid);
            }
        }

    }
    //遍历menuIds数组，获取角色id，同时和菜单id，向角色和权限的中间表插入
    private void addRoleAndMenu(int[] menuIds,Integer rid){
        roleDao.deleteRoleAndMenuByRoleId(rid);
        if (menuIds != null && menuIds.length > 0) {
            for (int menuId : menuIds) {
                roleDao.addRoleAndMenu(menuId,rid);
            }
        }
    }

}
