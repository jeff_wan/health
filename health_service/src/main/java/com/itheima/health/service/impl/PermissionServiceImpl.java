package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.PermissionDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Permission;
import com.itheima.health.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName CheckItemServiceImpl
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:54
 * @Version V1.0
 */
@Service(interfaceClass = PermissionService.class)
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    PermissionDao permissionDao;

    @Override
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        //分页初始化
        PageHelper.startPage(currentPage,pageSize);
        Page<Permission> page= permissionDao.findPage(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public void add(Permission permission) {
        permissionDao.add(permission);
    }

    @Override
    public Permission findById(Integer id) {
        return permissionDao.findById(id);
    }

    @Override
    public void edit(Permission permission) {
        permissionDao.edit(permission);
    }

    @Override
    public void delete(Integer id) {
        // 在删除之前，判断检查组和检查项中间表中是否存在数据，有数据不能删除，没有数据可以删除
        long count = permissionDao.findPermissionAndRole(id);
        // 此时存在数据，不能删除，提示用户
        if(count>0){
            throw new RuntimeException("中间表和检查组中存在关联数据，不能删除");
        }
        // 删除检查项
        permissionDao.deleteById(id);
    }

    @Override
    public List<Permission> findAllPermission() {
        return permissionDao.findAllPermission();
    }

}
