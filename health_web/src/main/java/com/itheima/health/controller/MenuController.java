package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constants.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.Menu;
import com.itheima.health.service.MenuService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Reference
    MenuService menuService;

    //分页查询
    @RequestMapping(value = "/findPageMenu")
    public PageResult findPageMenu(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = menuService.findPageMenu(queryPageBean.getCurrentPage(),queryPageBean.getPageSize(),queryPageBean.getQueryString());
        return pageResult;
    }

    // 新增
    @RequestMapping(value = "/addMenu")
    public Result addMenu(@RequestBody Menu menu){
        try {
            menuService.addMenu(menu);
            return new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKITEM_FAIL);
        }
    }

    //编辑查询
    @RequestMapping(value = "/findMenuById")
    public Result findMenuById(@RequestParam(value = "id") Integer id){
        try {
            Menu menu = menuService.findMenuById(id);
            return new Result(true, MessageConstant.QUERY_CHECKITEM_SUCCESS,menu);
        }catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKITEM_FAIL);
        }
    }

    // 编辑保存
    @RequestMapping(value = "/editMenu")
    public Result editMenu(@RequestBody Menu menu){
        try {
            menuService.editMenu(menu);
            return new Result(true, MessageConstant.EDIT_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_CHECKITEM_FAIL);
        }
    }

    // 删除
    @RequestMapping(value = "/deleteMenuById")
    public Result deleteMenuById(@RequestParam(value = "id") Integer id){
        try {
            menuService.deleteMenuById(id);
            return new Result(true, MessageConstant.DELETE_CHECKITEM_SUCCESS);
        } catch (RuntimeException e){
            return new Result(false, e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_CHECKITEM_FAIL);
        }
    }

    //查询所有
    @RequestMapping(value = "/findMenuAll")
    public Result findMenuAll(){
        List<Menu> list =  menuService.findMenuAll();
        if(list!=null && list.size()>0){
            return new Result(true,MessageConstant.FINDMENU_SUCCESS,list);
        }else{
            return new Result(false,MessageConstant.FINDMENU_FAIL);
        }
    }

    //查询菜单功能
    @RequestMapping(path = "/getMenuList")
    public List<Menu> getMenuList(String username){
        return menuService.getMenuList(username);
    }

}
