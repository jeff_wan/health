package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constants.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.Menu;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.service.MenuService;
import com.itheima.health.service.PermissionService;
import com.itheima.health.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Reference
    RoleService roleService;

    @Reference
    PermissionService permissionService;

    @Reference
    MenuService menuService;


    //分页查询
    @RequestMapping(value = "/findAllRole")
    public PageResult findAllRole(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = roleService.findAllRole(queryPageBean.getCurrentPage(),queryPageBean.getPageSize(),queryPageBean.getQueryString());
        return pageResult;
    }

    //查询权限
    @RequestMapping("/findAllPermission")
    public Result findAllCheckItem(){
        try {
            List<Permission> permissions = permissionService.findAllPermission();
            return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,permissions);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_CHECKITEM_FAIL);
        }
    }

    //菜单信息
    @RequestMapping("/findAllMenu")
    public Result findAllMenu(){
        try {
            List<Menu> menus = menuService.findMenuAll();
            return new Result(true,null,menus);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_MENU_FAIL);
        }
    }

    //添加
    @RequestMapping("/addRole")
    public Result addRole(int[] permissionIds,int[] menuIds, @RequestBody Role role){
        try {
            roleService.addRole(permissionIds,menuIds,role);
            return new Result(true,MessageConstant.ADD_ROLE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_ROLE_FAIL);
        }
    }

    // ID查询
    @RequestMapping(value = "/findRoleById")
    public Result findRoleById(Integer id){
        Role role = roleService.findRoleById(id);
        if(role!=null){
            return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,role);
        }
        return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
    }

    // 使用角色id，查询角色对检查项的集合，对应json的数据格式是：[28,29,30,31,32]
    @RequestMapping(value = "/findPermissionByRoleId")
    public List<Integer> findPermissionByRoleId(Integer id){
        List<Integer> permissionIds = roleService.findPermissionByRoleId(id);
        return permissionIds;
    }
    // 使用角色id，查询角色对角色项的集合，对应json的数据格式是：[28,29,30,31,32]
    @RequestMapping(value = "/findMenuByRoleId")
    public List<Integer> findMenuByRoleId(Integer id){
        List<Integer> menuIds = roleService.findMenuByRoleId(id);
        return menuIds;
    }

    //编辑添加
    @RequestMapping("/updateRole")
    public Result updateCheckGroup(@RequestBody Role role,int[] permissionIds,int[] menuIds){
        try {

            roleService.updateRole(role,permissionIds,menuIds);
            return new Result(true,MessageConstant.EDIT_ROLE_SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.EDIT_ROLE_FAIL);
        }
    }


    //删除检查项
    @RequestMapping(value = "/deleteRoleById")
    public Result deleteRoleById(Integer id){
        try {
            roleService.deleteRoleById(id);
        }catch (RuntimeException e) {
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true,MessageConstant.DELETE_CHECKITEM_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_CHECKITEM_SUCCESS);
    }

}
