package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealService {

    void add(Setmeal setmeal, Integer[] checkgroupIds);

    PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    /*List<Setmeal> findAll();

    Setmeal findById(Integer id);*/

    /*String findById(Integer id);

    String findAll();*/

    Object findById(Integer id);

    Object findAll();

    List<Map<String,Object>> findSetmealReport();

    Setmeal findBySetmealById(Integer id);

    List<Integer> findCheckGroupAndSetmealBySetmealId(Integer id);

    void update(Setmeal setmeal, Integer[] checkgroupIds);

    void deleteBySetmealId(Integer setmealId);

}
