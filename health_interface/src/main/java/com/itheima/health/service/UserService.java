package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.User;

public interface UserService {


    User findUserByUsername(String username);

    PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    User findById(Integer id);

    void edit(User user);

    void add(User user);

    void delete(Integer id);
}
