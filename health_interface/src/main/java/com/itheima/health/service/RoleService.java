package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Role;

import java.util.List;

public interface RoleService {

    PageResult findAllRole(Integer currentPage, Integer pageSize, String queryString);

    void addRole(int[] permissionIds, int[] menuIds, Role role);

    Role findRoleById(Integer id);

    List<Integer> findPermissionByRoleId(Integer id);

    List<Integer> findMenuByRoleId(Integer id);

    void deleteRoleById(Integer id);

    void updateRole(Role role, int[] permissionIds, int[] menuIds);
}