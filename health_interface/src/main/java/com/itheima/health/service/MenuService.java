package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Menu;

import java.util.List;

public interface MenuService {


    PageResult findPageMenu(Integer currentPage, Integer pageSize, String queryString);

    void addMenu(Menu menu);

    Menu findMenuById(Integer id);

    void editMenu(Menu menu);

    void deleteMenuById(Integer id);

    List<Menu> findMenuAll();
    List<Menu> getMenuList(String username);
}
