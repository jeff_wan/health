package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Member;

import java.util.List;
import java.util.Map;

public interface MemberService {

    Member findMemberByTelephone(String telephone);

    void add(Member member);

    List<Integer> findMemberCountByRegTime(List<String> monthsList);


    List<Map<String, Object>> findMemberSexReport();

    List<Map<String, Object>> findMemberAgeReport();

    PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    Member findById(Integer id);

    void edit(Member member);

    void delete(Integer id);
}
